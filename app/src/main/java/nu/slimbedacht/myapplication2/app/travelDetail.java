package nu.slimbedacht.myapplication2.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;


public class travelDetail extends Activity implements View.OnClickListener {

    private static AsyncFacebookRunner mAsyncRunner = null;
    Facebook fb;
    Button choosTripButton;
    private static String UserId;
    private ListView lv;

    final private static List<String> friendList = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travel_detail);

        fb = FacebookSingleton.create();

        getFriendlist();
        setUserdata();
        choosTripButton = (Button)findViewById(R.id.chooseTrip);

        choosTripButton.setOnClickListener(this);

    }

    private void getFriendlist() {

        mAsyncRunner = new AsyncFacebookRunner(fb);
        mAsyncRunner.request("me/friends", new AsyncFacebookRunner.RequestListener() {

            public void onComplete(String response, Object state) {
                //Log.d("friends-data:", response);
                String json = response;
                try {
                    JSONObject profile = new JSONObject(json);

                    JSONArray data = profile.getJSONArray("data");


                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jsonObject = data.getJSONObject(i);

                        String id = jsonObject.getString("id");

                        friendList.add(id);

                    }

                    useFriendList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            @Override
            public void onIOException(IOException e, Object state) {

            }

            @Override
            public void onFileNotFoundException(FileNotFoundException e, Object state) {

            }

            @Override
            public void onMalformedURLException(MalformedURLException e, Object state) {

            }

            @Override
            public void onFacebookError(FacebookError e, Object state) {

            }
        });
    }

     private void useFriendList(){

         Intent intent = getIntent();
         String travelId = intent.getStringExtra("travelId");

         String apiUrl = "http://www.slimbedacht.nu/ovapi/api.php?method=getAllUsers&"+travelId;
         AsyncHttpClient client = new AsyncHttpClient();
         RequestParams params = new RequestParams();

         //Log.d("click", "clicked");

         client.get(apiUrl, params, new AsyncHttpResponseHandler() {
             @Override
             public void onSuccess(String response) {
                Log.d("succes", response);
                setUsersList(response);


             }
         });

         //Log.d("friendlist", friendList.toString());
     }

    private void setUsersList(final String response){
        final List<String> TravelUsers = new ArrayList<String>();

        lv = (ListView) findViewById(R.id.listViewDetail);
            String  json = response;
            JSONArray profile;
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                TravelUsers);
        try {
            profile = new JSONArray(json);
            for (int i = 0; i < profile.length(); i++) {

                JSONObject jObj = profile.getJSONObject(i);
                final String userFirstName = jObj.getString("voornaam");
                final String userLastName = jObj.getString("achternaam");
                final String userId = jObj.getString("id");


                for(int j = 0; j < friendList.size(); j++) {
                    //Log.d("facebooktest", friendList.get(j));
                    Log.d("userid", friendList.get(j));
                    if(friendList.get(j).equals(userId)) {
                        Log.d("user= ", "True!!!");
                        TravelUsers.add(userFirstName + " " + userLastName);

                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        runOnUiThread(new Runnable() {
            public void run() {

                lv.setAdapter(arrayAdapter);
            }
        });





    }

    private void setUserdata(){
        mAsyncRunner = new AsyncFacebookRunner(fb);
        mAsyncRunner.request("me", new AsyncFacebookRunner.RequestListener() {

            public void onComplete(String response, Object state) {
                String json = response;
                Log.d("response", response);
                try {
                    JSONObject profile = new JSONObject(json);
                    String id = profile.optString("id");

                    UserId = id;
                    Log.d("id", UserId);

                    //R.layout.activity_travel_detail.choosTripButton.setVisibility(0);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onIOException(IOException e, Object state) {

            }

            @Override
            public void onFileNotFoundException(FileNotFoundException e, Object state) {

            }

            @Override
            public void onMalformedURLException(MalformedURLException e, Object state) {

            }

            @Override
            public void onFacebookError(FacebookError e, Object state) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.travel_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent intent = getIntent();
        String travelId = intent.getStringExtra("travelId");

        String apiUrl = "http://www.slimbedacht.nu/ovapi/api.php?method=addTripId&userid="+ UserId +"&"+travelId;
        Log.d("apitotravel", apiUrl);
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        //Log.d("click", "clicked");

        client.get(apiUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                Log.d("added to trip", "true");
                Toast.makeText(travelDetail.this, "Added to trip", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
