package nu.slimbedacht.myapplication2.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Settings;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.facebook.model.GraphUser;
import com.google.gson.Gson;
import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MainActivity extends Activity implements View.OnClickListener {

    Facebook fb;
    ImageView pic;
    com.facebook.widget.LoginButton button;
    private SharedPreferences sp;
    private static AsyncFacebookRunner mAsyncRunner = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fb = FacebookSingleton.create();

        sp = getPreferences(MODE_PRIVATE);
        String access_token = sp.getString("access_token", null);
        long expires = sp.getLong("access_expires", 0);

        if (access_token != null){
            fb.setAccessToken(access_token);
            Toast.makeText(MainActivity.this, "Loggedin", Toast.LENGTH_SHORT).show();
            startActivity(new Intent("android.intent.action.loggedinActivity"));
        }
        if (expires != 0){
            //Toast.makeText(MainActivity.this, "expired", Toast.LENGTH_SHORT).show();
            fb.setAccessExpires(expires);
        }

        button = (com.facebook.widget.LoginButton)findViewById(R.id.login);
        pic = (ImageView)findViewById(R.id.picture_pic);
        button.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void createUser(){
        mAsyncRunner = new AsyncFacebookRunner(fb);
        mAsyncRunner.request("me", new AsyncFacebookRunner.RequestListener() {

            public void onComplete(String response, Object state) {
                String json = response;
                Log.d("response", response);
                try {
                    JSONObject profile = new JSONObject(json);

                    String name = profile.optString("first_name");
                    String id = profile.optString("id");
                    String lastname = profile.optString("last_name");


                    String apiUrl = "http://www.slimbedacht.nu/ovapi/api.php?method=createUser&userid="+id+"&firstname="+name+"&lastname="+lastname;
                    Log.d("apiurl", apiUrl);
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();

                    //Log.d("click", "clicked");

                    client.get(apiUrl, params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String response) {
                            Log.d("succes", "true");
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            @Override
            public void onIOException(IOException e, Object state) {

            }

            @Override
            public void onFileNotFoundException(FileNotFoundException e, Object state) {

            }

            @Override
            public void onMalformedURLException(MalformedURLException e, Object state) {

            }

            @Override
            public void onFacebookError(FacebookError e, Object state) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(fb.isSessionValid()){
            try {
                fb.logout(getApplicationContext());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            //login to facebook
            fb.authorize(this, new Facebook.DialogListener() {
                @Override
                public void onComplete(Bundle values) {
                    Toast.makeText(MainActivity.this, "Loggedin", Toast.LENGTH_SHORT).show();

                    //save loggedin state
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("access_token", fb.getAccessToken());
                    editor.putLong("access_expires", fb.getAccessExpires());
                    editor.commit();

                    createUser();

                    startActivity(new Intent("android.intent.action.loggedinActivity"));

                }

                @Override
                public void onFacebookError(FacebookError e) {
                    Toast.makeText(MainActivity.this, "fbError", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(DialogError e) {
                    Toast.makeText(MainActivity.this, "OnError", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancel() {
                    Toast.makeText(MainActivity.this, "OnCancel", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }





    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        fb.authorizeCallback(requestCode, resultCode, data);
    }
}
