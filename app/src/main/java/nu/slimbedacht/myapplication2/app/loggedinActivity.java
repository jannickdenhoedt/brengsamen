package nu.slimbedacht.myapplication2.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class loggedinActivity extends Activity implements View.OnClickListener {

    Button button;
    private static List<String> autoCompleteList = new ArrayList<String>();
    private static List<String> autoCompleteListTo = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loggedin);
        button = (Button)findViewById(R.id.buttonGo);
        button.setOnClickListener(this);


        //check if text in inputfield is changed
        final AutoCompleteTextView textMessage = (AutoCompleteTextView)findViewById(R.id.editTextFrom);
        textMessage.addTextChangedListener(new TextWatcher()
        {
            public void afterTextChanged(Editable s) {

                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();

                client.get("http://api.9292.nl/0.1/locations?lang=nl-NL&q="+textMessage.getText().toString(), params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        //Log.d("succes-onchange", response);
                        try {
                            autoCompleteList.clear();
                            JSONObject resultObject = new JSONObject(response);
                            JSONArray data = resultObject.getJSONArray("locations");

                            if(data != null) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject jsonObject = data.getJSONObject(i);
                                    String locationId = jsonObject.getString("id");
                                    autoCompleteList.add(locationId);
                                }
                            }
                        //Log.d("getId", autoCompleteList.toString());
                        setAdapter();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                //Log.d("textFrom", "changes!!");
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        final AutoCompleteTextView textMessageTo = (AutoCompleteTextView)findViewById(R.id.editTextTo);
        textMessageTo.addTextChangedListener(new TextWatcher()
        {
            public void afterTextChanged(Editable s) {

                AsyncHttpClient clientTo = new AsyncHttpClient();
                RequestParams paramsTo = new RequestParams();

                clientTo.get("http://api.9292.nl/0.1/locations?lang=nl-NL&q="+textMessageTo.getText().toString(), paramsTo, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        //Log.d("succes-onchange", response);
                        try {
                            autoCompleteListTo.clear();
                            JSONObject resultObject = new JSONObject(response);
                            JSONArray data = resultObject.getJSONArray("locations");

                            if(data != null) {
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject jsonObject = data.getJSONObject(i);
                                    String locationId = jsonObject.getString("id");
                                    autoCompleteListTo.add(locationId);
                                }
                            }
                        //Log.d("getId", autoCompleteListTo.toString());
                        setAdapterTo();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                //Log.d("textTo", "changes!!");
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

    }

    private void setAdapter(){

        //sets adapter for autocomplete from input
        ArrayAdapter<String> adapterfrom = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, autoCompleteList);
        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.editTextFrom);
        textView.setAdapter(adapterfrom);

    }
    private void setAdapterTo(){

        //sets adapter for autocomplete to input
        ArrayAdapter<String> adapterto = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, autoCompleteListTo);
        AutoCompleteTextView textViewTo = (AutoCompleteTextView)
                findViewById(R.id.editTextTo);
        textViewTo.setAdapter(adapterto);
    }

@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.loggedin, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void findRoute(View v)
    {
        EditText from = (AutoCompleteTextView) findViewById(R.id.editTextFrom);
        EditText to = (AutoCompleteTextView) findViewById(R.id.editTextTo);

        Calendar now = Calendar.getInstance();
        int year = now.get(Calendar.YEAR);
        int day = now.get(Calendar.DAY_OF_MONTH);

        String yearInString = String.valueOf(year);
        String dayInString = String.valueOf(day);
        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        String timeCurrentHour;

        if(timePicker.getCurrentHour() < 10){
            timeCurrentHour = "0"+timePicker.getCurrentHour().toString();
        }
        else{
            timeCurrentHour = timePicker.getCurrentHour().toString();
        }

        String timeCurrentMinute;
        if(timePicker.getCurrentMinute() < 10){
            timeCurrentMinute = "0"+timePicker.getCurrentMinute().toString();
        }
        else{
            timeCurrentMinute = timePicker.getCurrentMinute().toString();
        }

        String apiUrl = "http://api.9292.nl/0.1/journeys?before=1&sequence=1&byFerry=true&bySubway=true&byBus=true&byTram=true&byTrain=true&lang=nl-NL&from="+from.getText().toString()+"&dateTime="+yearInString+"-04-"+dayInString+"T"+timeCurrentHour+timeCurrentMinute+"&searchType=departure&interchangeTime=standard&after=5&to="+to.getText().toString();
        Log.d("request", apiUrl);
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();

        //Log.d("click", "clicked");

        client.get(apiUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                Log.d("succes", "true");
                routeHandler.getResponse(response);
                startActivity(new Intent("android.intent.action.travelOverview"));
            }
        });

    }

    @Override
    public void onClick(View v) {

        switch(v.getId()) {
            case R.id.buttonGo:
                this.findRoute(v);
                Toast.makeText(loggedinActivity.this, "reisopties zoeken", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
