package nu.slimbedacht.myapplication2.app;

import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jannick on 17-4-2014.
 */
public class routeHandler {

    private static String responseString;
    private static List<String> travelDataList = new ArrayList<String>();
    private static List<String> travelIdList = new ArrayList<String>();

    public static void getResponse(String response) {

        responseString = response;
        JSONObject resultObject = null;
        try {
            resultObject = new JSONObject(responseString);
            JSONArray data = resultObject.getJSONArray("journeys");
            travelDataList.clear();

            if(data != null) {
                for(int i = 0 ; i < data.length() ; i++) {
                    JSONObject jsonObject = data.getJSONObject(i);
                    String departure = jsonObject.getString("departure");
                    String arrival = jsonObject.getString("arrival");
                    String travelId = jsonObject.getString("id");

                   /* //get into jsonobject to receive data
                    JSONObject fareInfo = (JSONObject)jsonObject.getJSONObject("fareInfo");
                    JSONArray legsData = (JSONArray)fareInfo.getJSONArray("legs");
                    JSONObject fares = (JSONObject)legsData.getJSONObject(0);
                    //to location
                    JSONObject to = (JSONObject)fares.getJSONObject("to");
                    JSONObject toPlace = (JSONObject)to.getJSONObject("place");
                    String toName = (String)toPlace.getString("name");

                    //from location
                    JSONObject from = (JSONObject)fares.getJSONObject("from");
                    JSONObject fromPlace = (JSONObject)from.getJSONObject("place");
                    String fromName = (String)fromPlace.getString("name");*/

                    //ArrayList<String> dataArray = new ArrayList<String>();
                    //put all data in array
                    //dataArray.add(fromName);
                    //dataArray.add(toName);
                    //dataArray.add();


                    travelDataList.add(departure.split("T")[1] + " -> " + arrival.split("T")[1]);
                    travelIdList.add(travelId);


                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public static  List<String> getTravelDataList(){
        return travelDataList;
    }
    public static  List<String> getTravelIdList(){
        return travelIdList;
    }
}
